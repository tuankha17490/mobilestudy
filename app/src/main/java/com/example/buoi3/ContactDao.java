package com.example.buoi3;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ContactDao {
    @Query("Select * from Contact")
    public List<Contact> getAll();
    @Insert
    public void insertContact(Contact ...contacts);
    @Delete
    public void removeContact(Contact contact);
    @Update
    public  void updateContact(Contact contact);
}
