package com.example.buoi3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
public class AddContactActivity extends AppCompatActivity {
    private Contact contact;
    private EditText name,phone,email;

    private boolean isEdit = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        name = findViewById(R.id.edit_name);
        phone = findViewById(R.id.edit_phone);
        email = findViewById(R.id.edit_email);
        Intent intent = getIntent();
        this.contact = (Contact) intent.getSerializableExtra("contact");
        if(this.contact != null) {
            name.setText(this.contact.getName());
            phone.setText(this.contact.getPhone());
            email.setText(this.contact.getEmail());
            isEdit = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                if(this.contact != null) {
                    if(this.contact.getName() != name.getText().toString()) this.contact.setName(name.getText().toString());
                    if(this.contact.getPhone() != phone.getText().toString()) this.contact.setPhone(phone.getText().toString());
                    if(this.contact.getEmail() != email.getText().toString()) this.contact.setEmail(email.getText().toString());
                }
                else {
                    this.contact = new Contact(name.getText().toString(),phone.getText().toString(),email.getText().toString(),"");
                }
                Intent intent = new Intent();
                intent.putExtra("newContact", this.contact);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}