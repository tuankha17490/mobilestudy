package com.example.buoi3;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class DetailContactActivity extends AppCompatActivity {
    private  Contact contact;
    private int position;
    private TextView name,email,phone;
    private ContactDatabase contactDatabase;
    private ContactDao contactDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contact);
        name = findViewById(R.id.detail_name);
        email = findViewById(R.id.detail_email);
        phone = findViewById(R.id.detail_phone);
        Intent intent = getIntent();
        this.contact = (Contact) intent.getSerializableExtra("contact");
        this.position = intent.getIntExtra("position",this.position);
        System.out.println("ID" + contact.getId());
        System.out.println("Name" + contact.getName());
        System.out.println("Email" + contact.getEmail());
        System.out.println("Phone" + contact.getPhone());
        name.setText(""+this.contact.getName());
        email.setText(""+this.contact.getEmail());
        phone.setText(""+this.contact.getPhone());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                Intent intent = new Intent(DetailContactActivity.this, AddContactActivity.class);
                intent.putExtra("contact", this.contact);
                startActivityForResult(intent, 123);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 123 && resultCode == RESULT_OK) {
            final Contact contact = (Contact) data.getSerializableExtra("newContact");
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    contactDatabase = ContactDatabase.getInstance(getApplicationContext());
                    contactDao = contactDatabase.contactDao();
                    contactDao.updateContact(contact);
                }
            });
            name.setText(""+contact.getName());
            email.setText(""+contact.getEmail());
            phone.setText(""+contact.getPhone());
            Intent intent = new Intent();
            intent.putExtra("newContact", contact);
            intent.putExtra("position", this.position);
            setResult(RESULT_OK, intent);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}