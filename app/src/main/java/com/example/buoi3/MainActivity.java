package com.example.buoi3;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ArrayList<Contact> arrayList;
    private MyAdapter myAdapter;
    private SearchView search;
    private ContactDatabase contactDatabase;
    private ContactDao contactDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rv_contact);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        arrayList = new ArrayList<Contact>();
//        arrayList.add(new Contact(1,"RithySay", "a@gmail.com", "0123456789", ""));
//        arrayList.add(new Contact(2,"Lê Vũ Tuấn Kha", "b@gmail.com", "0123456789", ""));
//        arrayList.add(new Contact(3,"Nguyen Hữu Quỳnh Như", "c@gmail.com", "0123456789", ""));
//        arrayList.add(new Contact(4,"Trương Quốc Bảo", "d@gmail.com", "0123456789", ""));
//        arrayList.add(new Contact(5,"Lê Khoa Nam", "e@gmail.com", "0123456789", ""));
//        arrayList.add(new Contact(6,"Thiện Konphap", "f@gmail.com", "0123456789", ""));
        myAdapter = new MyAdapter(arrayList, this);
        recyclerView.setAdapter(myAdapter);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                contactDatabase = ContactDatabase.getInstance(getApplicationContext());
                contactDao = contactDatabase.contactDao();
//                for(Contact contact: arrayList) {
//                    contactDao.insertContact(contact);
//                }
                List<Contact> list = contactDao.getAll();
                for(Contact contact: list) {
                    arrayList.add(contact);
                }
                myAdapter.notifyDataSetChanged();
            }
        });
    }
    public void AddContact(View v) {
        Intent intent = new Intent(MainActivity.this, AddContactActivity.class);
        startActivityForResult(intent, 1704);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == 111 && resultCode == RESULT_OK) {
            Contact updateContact = (Contact) data.getSerializableExtra("newContact");
            int position = data.getIntExtra("position",0);
            System.out.println("Main activity" + position);
            System.out.println("Main activity contact" + updateContact.getName());
            arrayList.set(position, updateContact);
            myAdapter.notifyDataSetChanged();
        }
        if(requestCode == 1704 && resultCode == RESULT_OK) {
            final Contact newContact = (Contact) data.getSerializableExtra("newContact");
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    contactDao.insertContact(newContact);
                }
            });
            arrayList.add(newContact);
            myAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_content, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                myAdapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }
}