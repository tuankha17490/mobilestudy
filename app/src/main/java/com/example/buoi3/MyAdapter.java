package com.example.buoi3;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> implements Filterable {
    private static ArrayList<Contact> mContacts;
    private ArrayList<Contact> mContactFull;
//    private static Context context;
    private MainActivity  mainActivity;
    private int currentClickPosition = -1;

//    public MyAdapter(ArrayList<Contact> mContacts, Context context) {
//        this.mContacts = mContacts;
//        mContactFull = new ArrayList<Contact>(mContacts);
//        this.context = context;
//    }
    public MyAdapter(ArrayList<Contact> mContacts, MainActivity mainActivity) {
        this.mContacts = mContacts;
        mContactFull = new ArrayList<Contact>(mContacts);
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textView.setText(mContacts.get(position).getName());

        if (position != currentClickPosition || position == -1) {
            holder.linearLayout.setVisibility(View.GONE);
        } else  {
            holder.linearLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Contact> filteredList = new ArrayList<Contact>();
            if(charSequence == null || charSequence.length() == 0) {
                filteredList.addAll(mContactFull);
            }
            else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (Contact c: mContactFull) {
                    if(c.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(c);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            mContacts.clear();
            mContacts.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;
        public View view;
        public LinearLayout linearLayout, linearParent,linearDel, linearCall, linearInfo;
        private ContactDatabase contactDatabase;
        private ContactDao contactDao;
        public MyViewHolder(View v) {
            super(v);
            view = v;
            textView = view.findViewById(R.id.text_view);
            linearLayout = view.findViewById(R.id.linear_info);
            linearParent = view.findViewById(R.id.parent);
            linearCall = view.findViewById(R.id.phone);
            linearDel = view.findViewById(R.id.delete);
            linearInfo = view.findViewById(R.id.info);
            linearParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    final Contact contact = mContacts.get(position);
                    if(linearLayout.getVisibility() == view.GONE) {
                        currentClickPosition = getAdapterPosition();
                        notifyDataSetChanged();
                    }
                    else  {
                        currentClickPosition = -1;
                        linearLayout.setVisibility(view.GONE);
                    }
                }
            });
            linearCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    String phoneNumber = mContacts.get(position).getPhone();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null));
                    mainActivity.startActivity(intent);
                }
            });
            linearDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    final Contact contact = mContacts.get(position);
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            contactDatabase = ContactDatabase.getInstance(mainActivity);
                            contactDao = contactDatabase.contactDao();
                            contactDao.removeContact(contact);
                        }
                    });
                    mContacts.remove(position);
                    notifyDataSetChanged();
                }
            });
            linearInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Contact contact = mContacts.get(position);
                    Intent intent = new Intent(mainActivity,DetailContactActivity.class);
                    intent.putExtra("contact", contact);
                    intent.putExtra("position", position);
                    mainActivity.startActivityForResult(intent, 111);
                }
            });

        }
    }
}
